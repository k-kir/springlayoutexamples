import javax.swing.*;
import java.awt.*;

public class Example3 {
    public static void main(String[] args) {
        JPanel panel = new JPanel(new SpringLayout());
        for (int i = 0; i < 9; i++) {
            JTextField textField = new JTextField(Integer.toString(i));
            //Делаем поле по центру длинным
            if (i == 4) {
                textField.setText("---Очень длинная строка---");
            }
            panel.add(textField);
        }

        //Создание структуры-сетки panel'и
        SpringUtilities.makeGrid(panel,
                3, 3, //строки, столбцы
                5, 5, //начальное X, начальное Y
                5, 5); //отступы по оси X, отступы по оси Y

        JFrame frame = new JFrame("Example3 - Сетка");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Настройка панели содержимого
        panel.setOpaque(true); //panel должна быть непрозрачной
        frame.setContentPane(panel);

        //Автоподбор размера
        frame.pack();

        //Установить минимальный размер
        frame.setMinimumSize(new Dimension(frame.getWidth(), frame.getHeight()));

        //Центрирование окна
        frame.setLocationRelativeTo(null);

        frame.setVisible(true);
    }
}
