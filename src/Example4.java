import javax.swing.*;
import java.awt.*;

public class Example4 {
    public static void main(String[] args) {
        JPanel panel = new JPanel(new SpringLayout());

        int rows = 10;
        int cols = 9;
        for (int r = 0; r < rows; r++) {
            for (int c = 1; c <= cols; c++) {
                int anInt = (int)Math.pow(r, c);
                JTextField textField = new JTextField(Integer.toString(anInt));
                panel.add(textField);
            }
        }

        //Создание структуры-компактной сетки panel'и
        SpringUtilities.makeCompactGrid(panel,
                rows, cols, //строки, столбцы
                3, 3, //начальное X, начальное Y
                3, 3); //отступы по оси X, отступы по оси Y

        JFrame frame = new JFrame("Example4 - Компактная сетка");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Настройка панели содержимого
        panel.setOpaque(true); //panel должна быть непрозрачной
        frame.setContentPane(panel);

        //Автоподбор размера
        frame.pack();

        //Установить минимальный размер
        frame.setMinimumSize(new Dimension(frame.getWidth(), frame.getHeight()));

        //Центрирование окна
        frame.setLocationRelativeTo(null);

        frame.setVisible(true);
    }
}
