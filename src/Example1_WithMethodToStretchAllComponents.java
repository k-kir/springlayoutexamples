import javax.swing.*;
import java.awt.*;

public class Example1_WithMethodToStretchAllComponents {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Example1 - Растягивание всех компонентов с помощью метода");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container contentPane = frame.getContentPane();
        SpringLayout layout = new SpringLayout();
        contentPane.setLayout(layout);
        JLabel label = new JLabel("Label: ");
        JTextField textField = new JTextField("Text field", 15);
        contentPane.add(label);
        contentPane.add(textField);

        //Помещение label в точку (5,5)
        SpringLayout.Constraints labelCons =
                layout.getConstraints(label);
        labelCons.setX(Spring.constant(5));
        labelCons.setY(Spring.constant(5));

        //Помещение textField правее label на 5 пикселей
        SpringLayout.Constraints textFieldCons =
                layout.getConstraints(textField);
        textFieldCons.setX(Spring.sum(Spring.constant(5),
                labelCons.getConstraint(SpringLayout.EAST)));
        textFieldCons.setY(Spring.constant(5));

        //Применение ограничений для contentPane
        setContainerSize(contentPane, 5);

        //Автоподбор размера
        frame.pack();

        //Установить минимальный размер
        frame.setMinimumSize(new Dimension(frame.getWidth(), frame.getHeight()));

        //Центрирование окна
        frame.setLocationRelativeTo(null);

        frame.setVisible(true);
    }

    public static void setContainerSize(Container parent, int pad) {
        SpringLayout layout = (SpringLayout) parent.getLayout();
        Component[] components = parent.getComponents();
        Spring maxHeightSpring = Spring.constant(0);
        SpringLayout.Constraints pCons = layout.getConstraints(parent);

        //Установить значение правой границы компонента значением правой границы его крайнего правого компонента + отступ
        Component rightmost = components[components.length - 1];
        SpringLayout.Constraints rCons =
                layout.getConstraints(rightmost);
        pCons.setConstraint(
                SpringLayout.EAST,
                Spring.sum(Spring.constant(pad),
                        rCons.getConstraint(SpringLayout.EAST)));

        //Установить значение нижней границы компонента значением нижней границы его крайнего нижнего компонента + отступ
        for (Component component : components) {
            SpringLayout.Constraints cons =
                    layout.getConstraints(component);
            maxHeightSpring = Spring.max(maxHeightSpring,
                    cons.getConstraint(
                            SpringLayout.SOUTH));
        }
        pCons.setConstraint(
                SpringLayout.SOUTH,
                Spring.sum(Spring.constant(pad), maxHeightSpring));
    }
}