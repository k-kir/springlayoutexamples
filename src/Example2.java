import javax.swing.*;
import java.awt.*;

public class Example2 {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Example2 - Относительное расположение элементов");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //frame.setSize(320, 200);
        Container contentPane = frame.getContentPane();
        SpringLayout layout = new SpringLayout();
        contentPane.setLayout(layout);

        //Создание кнопок
        Component b1 = new JButton("КНОПКА");
        Component b2 = new JButton("Кнопка пониже");
        Component b3 = new JButton("Кнопка ещё пониже");
        Component b4 = new JButton("------- Длинная кнопка -------");

        //Помещение кнопок на frame
        contentPane.add(b1);
        contentPane.add(b2);
        contentPane.add(b3);
        contentPane.add(b4);

        //Поставить кнопку b1 на позицию (25, 10) окна
        layout.putConstraint(SpringLayout.WEST, b1,
                25, SpringLayout.WEST, contentPane);
        layout.putConstraint(SpringLayout.NORTH, b1,
                10, SpringLayout.NORTH, contentPane);

        //Поставить кнопку b2 на 25 пикселей правее, чем b1 и на 10 пикселей ниже нижнего края b1
        layout.putConstraint(SpringLayout.WEST, b2,
                25, SpringLayout.WEST, b1);
        layout.putConstraint(SpringLayout.NORTH, b2,
                10, SpringLayout.SOUTH, b1);

        //Поставить кнопку b3 на 25 пикселей правее, чем b2 и на 10 пикселей ниже нижнего края b2
        layout.putConstraint(SpringLayout.WEST, b3,
                25, SpringLayout.WEST, b2);
        layout.putConstraint(SpringLayout.NORTH, b3,
                10, SpringLayout.SOUTH, b2);

        //Поставить кнопку b4 на 15 пикселей правее правого края b1 и на 10 пикселей ниже верхней границы окна
        layout.putConstraint(SpringLayout.WEST, b4,
                15, SpringLayout.EAST, b1);
        layout.putConstraint(SpringLayout.NORTH, b4,
                0, SpringLayout.NORTH, b1);

        //НЕ РАБОТАЕТ (кнопка уходит наверх при растягивании окна по высоте)
        //layout.putConstraint(SpringLayout.NORTH, b4,
        //        10, SpringLayout.NORTH, contentPane);

        //Растянуть окно на 25 пикселей вправо от правого края кнопки b4
        layout.putConstraint(SpringLayout.EAST, contentPane,
                25, SpringLayout.EAST, b4);

        //Растянуть окно на 10 пикселей вниз от нижнего края кнопки b3
        layout.putConstraint(SpringLayout.SOUTH, contentPane,
                10, SpringLayout.SOUTH, b3);

        //Автоподбор размера
        frame.pack();

        //Установить минимальный размер
        frame.setMinimumSize(new Dimension(frame.getWidth(), frame.getHeight()));

        //Центрирование окна
        frame.setLocationRelativeTo(null);

        frame.setVisible(true);
    }
}
