import javax.swing.*;
import java.awt.*;

public class Example5 {
    public static void main(String[] args) {
        String[] labels = {"Имя: ", "Телефон: ", "Email: ", "Адрес: "};
        int numPairs = labels.length;

        JPanel p = new JPanel(new SpringLayout());
        for (int i = 0; i < numPairs; i++) {
            JLabel l = new JLabel(labels[i], JLabel.TRAILING);
            p.add(l);
            JTextField textField = new JTextField(15);
            l.setLabelFor(textField);
            p.add(textField);
        }

        //Создание структуры-компактной сетки panel'и
        SpringUtilities.makeCompactGrid(p,
                numPairs, 2, //строки, столбцы
                6, 6, //начальное X, начальное Y
                6, 6); //отступы по оси X, отступы по оси Y

        JFrame frame = new JFrame("Example5 - Форма");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Настройка панели содержимого
        p.setOpaque(true); //panel должна быть непрозрачной
        frame.setContentPane(p);

        //Автоподбор размера
        frame.pack();

        //Установить минимальный размер
        frame.setMinimumSize(new Dimension(frame.getWidth(), frame.getHeight()));

        //Центрирование окна
        frame.setLocationRelativeTo(null);

        frame.setVisible(true);
    }
}
