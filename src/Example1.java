import javax.swing.*;
import java.awt.*;

public class Example1 {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Example1 - Растягивание");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container contentPane = frame.getContentPane();
        SpringLayout layout = new SpringLayout();
        contentPane.setLayout(layout);

        JLabel label = new JLabel("Label: ");
        contentPane.add(label);
        JTextField textField = new JTextField("Text field", 15);
        contentPane.add(textField);

        //Помещение label в точку (5,5)
        layout.putConstraint(SpringLayout.WEST, label,
                5,
                SpringLayout.WEST, contentPane);
        layout.putConstraint(SpringLayout.NORTH, label,
                5,
                SpringLayout.NORTH, contentPane);

        //Помещение textField правее label на 5 пикселей
        layout.putConstraint(SpringLayout.WEST, textField,
                5,
                SpringLayout.EAST, label);
        layout.putConstraint(SpringLayout.NORTH, textField,
                5,
                SpringLayout.NORTH, contentPane);

        //Растягивание textField на всё возможное пространство вправо и вниз с отступом 5 пикселей
        layout.putConstraint(SpringLayout.EAST, contentPane,
                5,
                SpringLayout.EAST, textField);
        layout.putConstraint(SpringLayout.SOUTH, contentPane,
                5,
                SpringLayout.SOUTH, textField);

        //Автоподбор размера
        frame.pack();

        //Установить минимальный размер
        frame.setMinimumSize(new Dimension(frame.getWidth(), frame.getHeight()));

        //Центрирование окна
        frame.setLocationRelativeTo(null);

        frame.setVisible(true);
    }
}
